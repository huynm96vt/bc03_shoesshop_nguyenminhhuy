import React, { Component } from 'react'

export default class Cart extends Component {
  render() {
    return (
      <div className='container'>
          <table className='table'>
              <thead>
                  <td>ID</td>
                  <td>Tên sản phẩm</td>
                  <td>Giá sản phẩm</td>
                  <td>Số lượng</td>
                  <td>Thao tác</td>
              </thead>
              <tbody>
                  {
                      this.props.gioHang.map((item) => {
                          return <tr>
                              <td>{item.id}</td>
                              <td>{item.name}</td>
                              <td>{item.price}</td>
                              <td>
                                  <button className='btn btn-success'>Tăng</button>
                                  <span className='mx-2'>{item?.soLuong}</span>
                                  <button className='btn btn-secondary'>Giảm</button>
                                  </td>
                                  
                              <td>
                                  <button onClick={() => {
                                      this.props.handleXoaSanPham(item.id)
                                  }} className='btn btn-danger'>Xoá</button>
                              </td>
                          </tr>
                      })
                  }
              </tbody>
          </table>
      </div>
    )
  }
}
