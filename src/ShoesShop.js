import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoeShop } from "./dataShoeShop";
import ProductList from "./ProductList";

export default class ShoesShop extends Component {
  state = {
    productList: dataShoeShop,
    gioHang: [],
  };

  handleAddToCart = (sanPham) => {
    let cloneGioHang = [...this.state.gioHang];
    let index = cloneGioHang.findIndex((item) => {
      return item.id == sanPham.id;
    });
    if (index == -1) {
      let newSanPham = { ...sanPham, soLuong: 1 };
      cloneGioHang.push(newSanPham);
    } else {
      cloneGioHang[index].soLuong++;
    }

    this.setState({ gioHang: cloneGioHang });
  };

  handleXoaSanPham = (idSanPham) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == idSanPham;
    });
    if (index !== -1) {
      let cloneGioHang = [...this.state.gioHang];
      cloneGioHang.splice(index, 1);
      this.setState({ gioHang: cloneGioHang });
    }
  };
  render() {
    return (
      <div>
        <ProductList
          handleThemSanPham={this.handleAddToCart}
          productList={this.state.productList}
        />

        <h4>Số lượng sản phẩm trong giỏ hàng: {this.state.gioHang.length}</h4>
        {this.state.gioHang.length > 0 && (
          <Cart
            handleXoaSanPham={this.handleXoaSanPham}
            gioHang={this.state.gioHang}
          />
        )}
        {/* {this.state.gioHang.length > 0 ? <Cart gioHang = {this.state.gioHang}/> : ""} */}
      </div>
    );
  }
}
